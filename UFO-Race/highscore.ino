int highscore[NUM_HIGHSCORE];
char name[NUM_HIGHSCORE][NAMELENGTH+1];

const uint16_t highscore_sound[] PROGMEM = {
  0x0005,0x140,0x150,0x15C,0x170,0x180,0x16C,0x154,0x160,0x174,0x184,0x14C,0x15C,0x168,0x17C,0x18C,0x0000};

bool drawNewHighscore(unsigned int score);
void drawHighScores();

void initHighscore(){
  for(byte thisScore = 0; thisScore < NUM_HIGHSCORE; thisScore++){
    highscore[thisScore] = gb_save_get(2*thisScore);
    gb_save_get_buf(2*thisScore + 1, name[thisScore], NAMELENGTH+1);
  }
}

void saveHighscore(unsigned int score){
  if(score < highscore[NUM_HIGHSCORE-1]){//if it's a highscore
    if(drawNewHighscore(score)){
      gb_getdefaultname(name[NUM_HIGHSCORE-1]);
      gb_gui_keyboard("NEW HIGHSCORE! NAME?", name[NUM_HIGHSCORE-1]);
      highscore[NUM_HIGHSCORE-1] = score;
      for(byte i=NUM_HIGHSCORE-1; i>0; i--){ //bubble sorting FTW
        if(highscore[i-1] > highscore[i]){
          char tempName[NAMELENGTH];
          strcpy(tempName, name[i-1]);
          strcpy(name[i-1], name[i]);
          strcpy(name[i], tempName);
          unsigned int tempScore;
          tempScore = highscore[i-1];
          highscore[i-1] = highscore[i];
          highscore[i] = tempScore;
        }
        else{
          break;
        }
      }
      for(byte thisScore = 0; thisScore < NUM_HIGHSCORE; thisScore++){
        gb_save_setNum(2*thisScore, highscore[thisScore]);
        gb_save_set(2*thisScore + 1, name[thisScore]);
      }
      drawHighScores();
    }
  }
  else{
//      String scoreStr = "Time: " + String(score);
//      gb_gui_popup(scoreStr, 20);
      gb_gui_popup_time(score, 12000);
  }
}

void drawHighScores(){
  while(true){
    if(gb_update()){
      gb_display_clear();
      gb_display_cursorx = 9+randRange(0,2);
      gb_display_cursory = 0+randRange(0,2);
      gb_display_println("BEST TIMES");
      gb_display_textwrap = false;
      gb_display_cursorx = 0;
      gb_display_cursory = gb_display_fontheight*2;
      for(byte thisScore=0; thisScore<NUM_HIGHSCORE; thisScore++){
        gb_display_cursorx = 2;
        gb_display_cursory = gb_display_fontheight*2 + gb_display_fontheight*thisScore;

        if(highscore[thisScore]==0)
          gb_display_print('-');
        else
          gb_display_print(name[thisScore]);

        gb_display_cursorx = gb_display_width()-4*gb_display_fontwidth - 5;
        gb_display_println(highscore[thisScore]);
      }
      if(gb_buttons_pressed(BUTTON_A) || gb_buttons_pressed(BUTTON_B)){
        gb_sound_playok();
        break;
      }
    }
  }
}

boolean drawNewHighscore(unsigned int score){
  gb_sound_playpattern(highscore_sound, 0);
  gb_sound_playok();
  int timer = 150;
  while(1){
    if(gb_update()){
      timer--;
      if(!timer){
        return true;
      }
      if(gb_buttons_released(BUTTON_B)){
        return true;
      }
      gb_display_clear();
      gb_display_cursorx = 0;
      gb_display_cursory = 0;
      gb_display_print("NEW HIGHSCORE!");
      gb_display_cursorx = 0+random(0,2);
      gb_display_cursory = 15+random(0,2);
      gb_display_setfontsize(3);
      gb_display_println(score);
      gb_display_setfontsize(1);
      
      gb_display_cursorx = 0;
      gb_display_cursory = gb_display_height() - (gb_display_fontheight * 5);
      gb_display_setcolor(LIGHTGREEN);
      gb_display_print("\nBest  ");
      gb_display_print(highscore[0]);
      gb_display_setcolor(RED);
      gb_display_print("\nWorst ");
      gb_display_print(highscore[NUM_HIGHSCORE-1]);
      
      gb_display_cursorx = 0;
      gb_display_cursory = gb_display_height() - (gb_display_fontheight * 1);
      gb_display_setcolor(GRAY);
      gb_display_print("B: SAVE");
    }
  }
}
