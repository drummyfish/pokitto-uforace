#define PAUSEMENULENGTH 2
const char strPlay[] = "START!";
const char strHighScores[] = "High scores";

const char* pauseMenu[PAUSEMENULENGTH] = {
  strPlay,
  strHighScores,
};

void drawHighScores();

void drawMenu(){
  gb_lights_clear();

  switch(gb_gui_menu("Menu", pauseMenu, PAUSEMENULENGTH)){
  case 0: //play
    initGame();
    play();
    break;
  case 1: //high scores
    drawHighScores();
    break;
  } 
}
