#include "gbsim.h"

//global variables
int camera_x, camera_y;

void initHighscore();
void drawMenu();
void initPlayer();
void initTime();
void updatePlayer();
void updateTime();
void drawWorld();
void drawMap();
void drawTime();
void drawPlayer();

/*
const SaveDefault savefileDefaults[] = {
  { 0, SAVETYPE_INT, 9999, 0 },
  { 1, SAVETYPE_BLOB, NAMELENGTH+1, 0 },
  { 2, SAVETYPE_INT, 9999, 0 },
  { 3, SAVETYPE_BLOB, NAMELENGTH+1, 0 },
  { 4, SAVETYPE_INT, 9999, 0 },
  { 5, SAVETYPE_BLOB, NAMELENGTH+1, 0 },
  { 6, SAVETYPE_INT, 9999, 0 },
  { 7, SAVETYPE_BLOB, NAMELENGTH+1, 0 },
  { 8, SAVETYPE_INT, 9999, 0 },
  { 9, SAVETYPE_BLOB, NAMELENGTH+1, 0 },
};
*/

void setup()
{
  gb_begin();
//  gb_save_config(savefileDefaults);
  
  initHighscore();
}

void loop(){
  drawMenu();
}

void initGame(){
  //gb.battery.show = false;
  initPlayer();
  initTime();
}

void play(){
  while(1){
    if(gb_update()){
      //pause the game if C is released
      if(gb_buttons_released(BUTTON_C)){
        return;
      }

      updatePlayer();
      updateTime();

      gb_display_setcolor(WHITE);
      gb_display_fill();
      gb_display_setcolor(BLACK, WHITE);
      drawWorld();
      drawMap();
      drawTime();
      drawPlayer();
      drawMetaMode();
    }
  }
}
