#!/bin/bash

clear; clear

make
#make 2>&1 >/dev/null | grep --color "error" | head -n10
#make 2> /dev/null

if [ $? -eq 0 ]; then
  ./PokittoEmu ./BUILD/firmware.bin $1
fi
